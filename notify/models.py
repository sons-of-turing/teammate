from django.db import models
from users.models import User
from users.models import Team


class Notice(models.Model):

    notice = "notice"
    req_to_team = "reqToTeam"
    req_from_team = "reqFromTeam"
    TYPE_CHOICES = (
        (req_to_team, "reqToTeam"),
        (req_from_team, "reqFromTeam"),
        (notice, "notice")
    )
    type = models.CharField(
        max_length=30, choices=TYPE_CHOICES, default=notice)
    from_user = models.ManyToManyField("users.User")
    to_team = models.ManyToManyField("users.Team")
    user_to_notify = models.ManyToManyField("users.User", related_name="user_to_notify")

    def __str__(self):
        return self.type
