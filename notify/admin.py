from django.contrib import admin
from .models import Notice


@admin.register(Notice)
class Notices(admin.ModelAdmin):
    list_display = ('id', 'type')
