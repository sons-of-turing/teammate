from . import views
from django.contrib import admin
from django.urls import path
from django.urls import include
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path(r'admin/', admin.site.urls),
    url(r'^$', views.index, name='index'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^events/$', views.events, name='events'),
    url(r'^teams/$', views.search_teams, name='search_teams'),
    url(r'^users/$', views.search_teammates, name='search_teammates'),
    url(r'^add/event/$', views.add_event, name='add_event'),
    url(r'^add/team/$', views.add_team, name='add_team'),
    url(r'^notify/$', views.notify, name='notify'),
    # url(r'^users/(\w+)/$', views.about_users, name='about_users'),
    url(r'^events/about/(\d+)/$', views.about_event, name='about_event'),
    url(r'^events/about/(\d+)/wishes/users/$',
        views.event_wishes_users, name='event_wishes_users'),
    url(r'^events/about/(\d+)/wishes/teams/$',
        views.event_wishes_teams, name='event_wishes_teams'),
    # url(r'teams/$', views.teams, name='teams'),
    url(r'^teams/(\w+)/$', views.about_team, name='about_team'),
    url(r'^teams/(\w+)/settings/$', views.team_settings, name='team_settings'),
    url(r'^teams/(\w+)/add/project/$',
        views.add_team_project, name='add_team_project'),
    url(r'^teams/(\w+)/teammates/$', views.team_teammates, name='team_teammates'),
    # url(r'^favourites/$', views.favourites, name='favourites'),
    url(r'^settings/$', views.settings, name='settings'),
    url(r'^im/$', views.profile, name='profile'),
    url(r'^im/teams/$', views.user_teams, name='user_teams'),
    url(r'^user/(\w+)/$', views.about_users, name='about_users'),
    url(r'^user/(\w+)/teams/$', views.about_user_teams, name='about_user_teams'),
    url(r'^select2/', include('django_select2.urls')),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
