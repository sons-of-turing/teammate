from django.shortcuts import render
from .models import Event
from users.models import User, Team, TeamProject
from notify.models import Notice
from taggit.models import Tag
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt


def index(request):

    if request.user.is_authenticated:
        return HttpResponseRedirect('/im/')
    else:
        return HttpResponseRedirect('/events/')


def events(request):

    events = Event.objects.order_by(
        "-id").filter(Q(is_approved=True))

    contentType = ContentType.objects.get_for_model(Event)
    available_tags = Tag.objects.filter(
        taggit_taggeditem_items__content_type=contentType).distinct()

    if request.GET.get('eventType'):

        # selectCity = request.GET.get('selectCity')
        eventsType = request.GET.get('eventType')
        skills = request.GET.getlist('tags[]')
        dateStart = request.GET.get('dateStart')
        dateFinish = request.GET.get('dateFinish')
        matchUserSkills = request.GET.get('matchUserSkills')

       # if selectCity:
       #     events = events.filter(city=selectCity)
        if eventsType and eventsType != 'Все':

            events = events.filter(type_of_event=eventsType)
        if skills[0] and skills[0] != '0' and matchUserSkills != '1':

            events = events.filter(tags__name__in=skills).distinct()
        if dateStart:
            events = events.filter(
                date_start__gte=dateStart,)
        if dateFinish:
            events = events.filter(
                date_start__lte=dateFinish)
        if matchUserSkills and matchUserSkills == '1':
            events = events.filter(
                tags__name__in=list(request.user.tags.all())).distinct()
        context = {
            'events': events,
        }

        return render(request, "events_page/all_events.htm", context)
    context = {
        'events': events,
        'all_tags': available_tags,
        'active_sidebar': 'events',
    }
    return render(request, "events_page/events.htm", context)


@csrf_exempt
def about_event(request, event_id):
    event = Event.objects.filter(id=event_id)

    if event[0].tags:
        similar_events = Event.objects.order_by(
            "-id").filter(~Q(id=event_id) & Q(is_approved=True)).filter(tags__name__in=list(event[0].tags.all())).distinct()[:3]
    user_teams = Team.objects.filter(users__id=request.user.id)
    context = {'event': event[0],
               'similar_events': similar_events,
               'active_sidebar': 'events',
               'user_teams': user_teams,
               }
    context['flag'] = not request.user in event[0].possible_users.all(
    ) and not user_teams.intersection(event[0].possible_teams.all())
    if request.method == "POST":
        cmd = request.POST.get('cmd', None)
        if cmd:
            if cmd == 'add':
                fav_user = request.user
                fav_user.favourite_events.add(event_id)
                fav_user.save()
            elif cmd == 'remove':
                fav_user = request.user
                fav_user.favourite_events.remove(event_id)
                fav_user.save()
            return render(request, "about_event_page/blocks/favourites_btn.htm", context)
        if request.POST['want']:

            if request.POST['want'] == "yes":
                if request.POST['type'] == "user":
                    event[0].possible_users.add(request.user.id)
                elif request.POST['type'] == "team" and request.POST['id'] != '':
                    event[0].possible_teams.add(request.POST['id'])
                context['do_cancel'] = True
            elif request.POST['want'] == "no":
                event[0].possible_users.remove(request.user.id)
                for t in user_teams.all():
                    event[0].possible_teams.remove(t.id)
            context['flag'] = not request.user in event[0].possible_users.all(
            ) and not user_teams.intersection(event[0].possible_teams.all())

            return render(request, "about_event_page/blocks/want_take_part_btn.htm", context)

    return render(request, "about_event_page/about_event_page.htm", context)


def event_wishes_users(request, event_id):
    event = Event.objects.filter(id=event_id)
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'events'})

    if request.GET:
        if request.GET.get('skills[]'):
            users = event[0].possible_users.all()
            selectCity = request.GET.get('selectCity')
            skills = request.GET.getlist('skills[]')
            selectTarget = request.GET.get('selectTarget')
            searchTeammates = request.GET.get('searchTeammates')
            name = searchTeammates
            surname = searchTeammates
            if searchTeammates or searchTeammates == "":
                if ' ' in searchTeammates:
                    search = searchTeammates.split()
                    name = search[0]
                    if(search[1] != ''):
                        surname = search[1]
                users = users.filter(Q(first_name__contains=name) |
                                     Q(last_name__contains=surname) |
                                     Q(username__contains=searchTeammates)).distinct()
            if selectCity:
                users = users.filter(city=selectCity).distinct()
            if skills[0] and skills[0] != '0':
                users = users.filter(tags__name__in=skills).distinct()
            if selectTarget and selectTarget != 'Выберите цель':
                users = users.filter(target=selectTarget)

        context = {
            'all_users': users,
        }
        return render(request, 'search_page/teammates/all_teammates.htm', context)

    else:
        contentType = ContentType.objects.get_for_model(User)
        available_skills = Tag.objects.filter(
            taggit_taggeditem_items__content_type=contentType).distinct()
        context = {'all_users': event[0].possible_users.all(),
                   'all_skills': available_skills,
                   'event': event[0],
                   'active_sidebar': 'events'
                   }
        return render(request, 'about_event_page/wishes_users.htm', context)


def event_wishes_teams(request, event_id):
    event = Event.objects.filter(id=event_id)
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'events'})

    if request.GET:

        if request.GET.get('skills[]'):
            teams = event[0].possible_teams.all()
            skills = request.GET.getlist('skills[]')
            selectTarget = request.GET.get('selectTarget')
            selectLevel = request.GET.get('selectLevel')
            searchTeams = request.GET.get('searchTeams')
            # if selectCity:
            #     teams = teams.filter(city=selectCity)
            if searchTeams or searchTeams == "":
                searchTeams = request.GET.get('searchTeams')
                teams = teams.filter(Q(name_id__contains=searchTeams) | Q(
                    name__icontains=searchTeams)).distinct()
            if skills[0] and skills[0] != '0':
                teams = teams.filter(
                    required_skills__name__in=skills).distinct()
            if selectTarget and selectTarget != 'Выберите цель':
                teams = teams.filter(target=selectTarget)
            if selectLevel and selectLevel != 'Выберите уровень':
                teams = teams.filter(level=selectLevel)
            if request.GET.get('orderByTCount'):
                orderBy = request.GET.get('orderByTCount')
                if orderBy == 'up':
                    teams = sorted(teams,  key=lambda m: m.count_of_members)
                elif orderBy == 'down':
                    teams = sorted(
                        teams,  key=lambda m: m.count_of_members, reverse=True)

            context = {'all_teams': teams}
            return render(request, 'search_page/teams/all_teams.htm', context)

    else:
        contentType = ContentType.objects.get_for_model(User)
        available_skills = Tag.objects.filter(
            taggit_taggeditem_items__content_type=contentType).distinct()
        context = {'all_teams': event[0].possible_teams.all(),
                   'event': event[0],
                   'all_skills': available_skills,
                   'active_sidebar': 'events',

                   }
        return render(request, 'about_event_page/wishes_teams.htm', context)


def about_users(request, username):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'profile'})
    if username == request.user.username:
        return HttpResponseRedirect('/im/')
    user = User.objects.filter(username=username)
    if user:
        context = {
            'about_user': user[0],
            'about_user_teams': Team.objects.filter(users__id=user[0].id),
            'active_sidebar': 'teammates'
        }
        return render(request, 'about_user_page/about_user.htm', context)
    else:
        return HttpResponse('<h5>NONE</h5>')


def profile(request):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'profile'})
    teams = Team.objects.filter(
        users__id=request.user.id)
    contentType = ContentType.objects.get_for_model(User)
    all_tags = Tag.objects.filter(
        taggit_taggeditem_items__content_type=contentType).distinct()
    context = {
        'user_teams': teams,
        'active_sidebar': 'profile',
        'all_tags': all_tags
    }
    if request.method == "POST":
        for tag in request.POST.getlist('user_tags'):
            request.user.tags.add(tag)

    return render(request, "main_page/content.htm", context)


def settings(request):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'profile'})
    contentType = ContentType.objects.get_for_model(User)
    all_tags = Tag.objects.filter(
        taggit_taggeditem_items__content_type=contentType).distinct()
    context = {
        'all_tags': all_tags,
        'user_tags':  list(request.user.tags.all().values_list('name', flat=True)),
        'active_sidebar': 'profile'
    }

    if request.method == "POST":
        user = request.user
        user.first_name = request.POST['user_name']
        user.last_name = request.POST['user_lastname']
        user.vk = request.POST['user_vk']
        user.tg = request.POST['user_tg']
        if request.POST['user_target'] != "Выберите цель":
            user.target = request.POST['user_target']
        else:
            user.target = None

        if User.objects.filter(username=request.POST['user_username']).exists() and user.username != request.POST['user_username']:
            return render(request, 'account_settings/messages/existing_nickname.htm', context)
        else:
            user.username = request.POST['user_username']
        if 'user_image' in request.FILES:
            user.image = request.FILES['user_image']
        user.save()

        if list(user.tags.all()) != request.POST.getlist('user_tags'):
            for tag in user.tags.all():
                user.tags.remove(tag)
            for tag in request.POST.getlist('user_tags'):
                user.tags.add(tag)

        # success = "Изменения успешно сохранены!"
        # context['success'] = success

        # return render(request, 'account_settings/messages/success.htm', context)
        return HttpResponseRedirect("/settings/")

    return render(request, 'account_settings/settings_block.htm', context)


def search_teams(request):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'teams'})

    teams = Team.objects.filter(~Q(users=request.user))

    contentType = ContentType.objects.get_for_model(Team)
    available_skills = Tag.objects.filter(
        taggit_taggeditem_items__content_type=contentType).distinct()

    if request.GET:

        if request.GET.get('skills[]'):

            skills = request.GET.getlist('skills[]')
            selectTarget = request.GET.get('selectTarget')
            selectLevel = request.GET.get('selectLevel')
            searchTeams = request.GET.get('searchTeams')
            # if selectCity:
            #     teams = teams.filter(city=selectCity)
            if searchTeams or searchTeams == "":
                searchTeams = request.GET.get('searchTeams')
                teams = teams.filter(Q(name_id__contains=searchTeams) | Q(
                    name__icontains=searchTeams)).distinct()
                print("SEARCH")
            if skills[0] and skills[0] != '0':
                teams = teams.filter(
                    required_skills__name__in=skills).distinct()
            if selectTarget and selectTarget != 'Выберите цель':
                teams = teams.filter(target=selectTarget)
            if selectLevel and selectLevel != 'Выберите уровень':
                teams = teams.filter(level=selectLevel)
            if request.GET.get('orderByTCount'):
                orderBy = request.GET.get('orderByTCount')
                if orderBy == 'up':
                    teams = sorted(teams,  key=lambda m: m.count_of_members)
                elif orderBy == 'down':
                    teams = sorted(
                        teams,  key=lambda m: m.count_of_members, reverse=True)

            context = {'all_teams': teams}
            return render(request, 'search_page/teams/all_teams.htm', context)

    else:
        contentType = ContentType.objects.get_for_model(User)
        available_skills = Tag.objects.filter(
            taggit_taggeditem_items__content_type=contentType).distinct()
        context = {'all_teams': teams,
                   'all_skills': available_skills,
                   'active_sidebar': 'teams'
                   }

        return render(request, 'search_page/teams/teams.htm', context)


def search_teammates(request):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'teammates'})

    users = User.objects.filter(~Q(username=request.user.username))

    contentType = ContentType.objects.get_for_model(User)
    available_skills = Tag.objects.filter(
        taggit_taggeditem_items__content_type=contentType).distinct()
    if request.GET:
        if request.GET.get('skills[]'):
            selectCity = request.GET.get('selectCity')
            skills = request.GET.getlist('skills[]')
            selectTarget = request.GET.get('selectTarget')
            searchTeammates = request.GET.get('searchTeammates')
            name = searchTeammates
            surname = searchTeammates
            if searchTeammates or searchTeammates == "":
                if ' ' in searchTeammates:
                    search = searchTeammates.split()
                    name = search[0]
                    if(search[1] != ''):
                        surname = search[1]
                users = users.filter(Q(first_name__contains=name) |
                                     Q(last_name__contains=surname) |
                                     Q(username__contains=searchTeammates)).distinct()
            if selectCity:
                users = users.filter(city=selectCity).distinct()
            if skills[0] and skills[0] != '0':
                users = users.filter(tags__name__in=skills).distinct()
            if selectTarget and selectTarget != 'Выберите цель':
                users = users.filter(target=selectTarget)

        context = {
            'all_users': users,
        }
        return render(request, 'search_page/teammates/all_teammates.htm', context)

    else:
        # all_users.filter(birth_date__lt=date.today() -
        #                  relativedelta(years=+18))
        context = {'all_users': users,
                   'all_skills': available_skills,
                   'active_sidebar': 'teammates'
                   }
        return render(request, 'search_page/teammates/teammates.htm', context)


def add_team(request):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'profile'})
    # contentType = ContentType.objects.get_for_model(Event)
    # all_tags_event = Tag.objects.filter(
    #     taggit_taggeditem_items__content_type=contentType).distinct()
    contentType = ContentType.objects.get_for_model(User)
    all_tags_team = Tag.objects.filter(
        taggit_taggeditem_items__content_type=contentType).distinct()
    context = {
        'all_tags_team': all_tags_team,
        'active_sidebar': 'profile',
    }

    if request.method == "POST":
        if request.POST['new_team_name']:
            new_team_name = request.POST['new_team_name']
            if request.POST['new_team_id']:
                new_team_id = request.POST['new_team_id']
            else:
                new_team_id = None
            print(new_team_id)
            new_team_target = request.POST['new_team_target']
            new_team_skills = request.POST.getlist('new_team_skills')
            new_team = Team(name=new_team_name,
                            target=new_team_target, admin=request.user.username)
            if new_team_id:
                new_team.name_id = new_team_id
            new_team.save()
            new_team.users.add(request.user.id)
            new_team.save()
            for tag in new_team_skills:
                new_team.required_skills.add(tag)

            success = "Команда успешно создана!"
            context['success'] = success

        return render(request, 'main_page/messages/success_add_team.htm', context)
    return render(request, 'main_page/add_team.htm', context)


def add_event(request):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm')
    contentType = ContentType.objects.get_for_model(Event)
    all_tags_event = Tag.objects.filter(
        taggit_taggeditem_items__content_type=contentType).distinct()
    context = {
        'all_tags_event': all_tags_event
    }
    if request.method == "POST":
        if request.POST.get('new_event_name'):
            new_event_name = request.POST['new_event_name']
            new_event_desc = request.POST['new_event_desc']
            new_event_tag = request.POST.getlist('new_event_tag')
            new_event_link = request.POST['new_event_link']
            new_event_image = request.FILES['new_event_image']
            new_event_dateS = request.POST['new_event_dateS']
            new_event_dateF = request.POST['new_event_dateF']
            new_event = Event(name=new_event_name,
                              description=new_event_desc, link=new_event_link, image=(new_event_image), date_start=new_event_dateS, date_finish=new_event_dateF)
            new_event.save()
            if new_event_tag:
                for tag in new_event_tag:
                    new_event.tags.add(tag)

            success = "Ваше мероприятие успешно отправлено на проверку!"
            context['success'] = success

            return render(request, 'main_page/messages/success_add_event.htm', context)

    return render(request, 'main_page/add_event.htm', context)


@csrf_exempt
def about_team(request, team_name_id):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'teams'})
    team = Team.objects.filter(name_id=team_name_id)

    context = {
        'team': team[0],
        'active_sidebar': 'teams'
    }

    if request.method == "POST":
        cmd = request.POST.get('cmd')
        if cmd:

            if cmd == 'request':

                # from_user = User.objects.filter(id=from_user_id)
                # to_team = Team.objects.filter(id=to_team_id)
                join_req = Notice(type="reqToTeam")
                join_req.save()
                join_req.user_to_notify.add(
                    User.objects.filter(username=team[0].admin)[0])
                join_req.from_user.add(request.user.id)
                join_req.to_team.add(team[0].id)
            elif cmd == 'cancel':
                # cancel_user = User.objects.filter(id=cancel_user_id)
                # cancel_team = Team.objects.filter(id=cancel_team_id)
                Notice.objects.filter(type="reqToTeam",
                                      from_user=request.user.id, to_team=team[0].id).delete()

            notice = Notice.objects.filter(type="reqToTeam",
                                           from_user=request.user.id, to_team=team[0].id)
            if notice:
                context['notice'] = notice[0]
                team_admin = User.objects.filter(
                    username=team[0].admin)[0]
                team_admin.is_new_notice = True
                team_admin.save()
            else:
                context['notice'] = None

        return render(request, "about_team_page/join_btn.htm", context)

    notice = Notice.objects.filter(type="reqToTeam",
                                   from_user=request.user.id, to_team=team[0].id)
    if notice:
        context['notice'] = notice[0]
    else:
        context['notice'] = None

    return render(request, 'about_team_page/about_team.htm', context)


@csrf_exempt
def notify(request):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm')
    if request.method == "POST":
        cmd = request.POST.get('cmd')
        if cmd:
            team_id = request.POST['team_id']
            user_to_join_id = request.POST['user_id']

            if cmd == 'accept':
                # from_user = User.objects.filter(id=from_user_id)
                # to_team = Team.objects.filter(id=to_team_id)
                team_to_join = Team.objects.filter(id=team_id)
                team_to_join[0].users.add(user_to_join_id)

            Notice.objects.filter(type="reqToTeam",
                                  from_user=user_to_join_id, to_team=team_id,
                                  user_to_notify=request.user).delete()
        context = {
            'notices': Notice.objects.filter(user_to_notify=request.user),
        }
        return render(request, "notices_page/all_notices.htm", context)
    context = {
        'notices':  None}
    if request.user.is_authenticated:
        user_admin = request.user
        user_admin.is_new_notice = False
        user_admin.save()
        context = {
            'notices': Notice.objects.filter(user_to_notify=request.user),
        }

    return render(request, "notices_page/notice.htm", context)


def team_settings(request, team_name_id):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'teams'})
    contentType = ContentType.objects.get_for_model(User)
    all_tags_team = Tag.objects.filter(
        taggit_taggeditem_items__content_type=contentType).distinct()
    team = Team.objects.filter(name_id=team_name_id)

    context = {
        'all_tags': all_tags_team,
        'team_tags': list(team[0].required_skills.all().values_list('name', flat=True)),
        'team': team[0],
        'active_sidebar': 'teams'
    }
    if request.method == "POST":
        if request.POST['team_name']:

            team_name = request.POST['team_name']

            team_id = request.POST['team_id']
            team_target = request.POST['team_target']
            team_skills = request.POST.getlist('team_tags')
            if team[0].name != team_name:
                team.update(name=team_name)
            if team[0].target != team_target:
                team.update(target=team_target)
            if team[0].name_id != team_id:
                team.update(name_id=team_id)
            team = Team.objects.filter(name_id=team_id)
            if list(team[0].required_skills.all()) != team_skills:
                for tag in team[0].required_skills.all():
                    team[0].required_skills.remove(tag)
                for tag in team_skills:
                    team[0].required_skills.add(tag)

            # success = "Изменения успешно сохранены!"
            # context['success'] = success

        return HttpResponseRedirect("/teams/" + team_id + "/settings/")
    return render(request, 'team_settings/settings.htm', context)


def team_teammates(request, team_name_id):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'teams'})
    team = Team.objects.filter(name_id=team_name_id)[0]

    if request.GET:
        if request.GET.get('skills[]'):
            users = team.users.all()
            selectCity = request.GET.get('selectCity')
            skills = request.GET.getlist('skills[]')
            selectTarget = request.GET.get('selectTarget')
            searchTeammates = request.GET.get('searchTeammates')
            name = searchTeammates
            surname = searchTeammates
            if searchTeammates or searchTeammates == "":
                if ' ' in searchTeammates:
                    search = searchTeammates.split()
                    name = search[0]
                    if(search[1] != ''):
                        surname = search[1]
                users = users.filter(Q(first_name__contains=name) |
                                     Q(last_name__contains=surname) |
                                     Q(username__contains=searchTeammates)).distinct()
            if selectCity:
                users = users.filter(city=selectCity).distinct()
            if skills[0] and skills[0] != '0':
                users = users.filter(tags__name__in=skills).distinct()
            if selectTarget and selectTarget != 'Выберите цель':
                users = users.filter(target=selectTarget)

        context = {
            'all_users': users,
        }
        return render(request, 'search_page/teammates/all_teammates.htm', context)

    else:

        contentType = ContentType.objects.get_for_model(User)
        available_skills = Tag.objects.filter(
            taggit_taggeditem_items__content_type=contentType).distinct()
        context = {'all_users':  team.users.all(),
                   'all_skills': available_skills,
                   'team': team,
                   'active_sidebar': 'teams'
                   }
        return render(request, 'team_teammates/team_teammates.htm', context)


def user_teams(request):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'profile'})
    user_teams = Team.objects.order_by(
        "-id").filter(users__id=request.user.id).all()
    if request.GET.get('skills[]'):
        filtered_teams = user_teams

        skills = request.GET.getlist('skills[]')

        if skills and skills[0] != '0' and skills[0] != 'give_me_sort_by_user_skills':
            filtered_teams = filtered_teams.filter(
                required_skills__name__in=skills).distinct()
        elif skills[0] == 'give_me_sort_by_user_skills':

            filtered_teams = filtered_teams.filter(
                required_skills__name__in=list(request.user.tags.all())).distinct()
        else:
            filtered_teams = user_teams
        context = {'all_teams': filtered_teams
                   }
        return render(request, 'main_page/user_teams/all_teams.htm', context)

    else:

        contentType = ContentType.objects.get_for_model(User)
        available_skills = Tag.objects.filter(
            taggit_taggeditem_items__content_type=contentType).distinct()

        context = {'all_teams': user_teams,
                   'all_skills': available_skills,
                   'active_sidebar': 'profile',
                   'user_of_teams': request.user.username,
                   }

    return render(request, 'main_page/user_teams/user_teams.htm', context)


def about_user_teams(request, username):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'teammates'})

    if request.GET:
        if request.GET.get('skills[]'):
            teams = Team.objects.order_by(
                "-id").filter(users__username=username)
            skills = request.GET.getlist('skills[]')
            selectTarget = request.GET.get('selectTarget')
            searchTeams = request.GET.get('searchTeams')
            # if selectCity:
            #     teams = teams.filter(city=selectCity)
            if searchTeams or searchTeams == "":
                searchTeams = request.GET.get('searchTeams')
                teams = teams.filter(Q(name_id__contains=searchTeams) | Q(
                    name__icontains=searchTeams)).distinct()
                print("SEARCH")
            if skills[0] and skills[0] != '0':
                teams = teams.filter(
                    required_skills__name__in=skills).distinct()
            if selectTarget and selectTarget != 'Выберите цель':
                teams = teams.filter(target=selectTarget)
            if request.GET.get('orderByTCount'):
                orderBy = request.GET.get('orderByTCount')
                if orderBy == 'up':
                    teams = sorted(teams,  key=lambda m: m.count_of_members)
                elif orderBy == 'down':
                    teams = sorted(
                        teams,  key=lambda m: m.count_of_members, reverse=True)

            context = {'all_teams': teams}
            return render(request, 'search_page/teams/all_teams.htm', context)

    else:

        contentType = ContentType.objects.get_for_model(User)
        available_skills = Tag.objects.filter(
            taggit_taggeditem_items__content_type=contentType).distinct()

        context = {'all_teams': Team.objects.order_by(
            "-id").filter(users__username=username),
            'all_skills': available_skills,
            'user_of_teams': username,
            'active_sidebar': 'teammates'
        }

        return render(request, 'main_page/user_teams/user_teams.htm', context)


def add_team_project(request, team_name_id):
    if not request.user.is_authenticated:
        return render(request, 'main_page/not_auth_user.htm', {'active_sidebar': 'teams'})
    # contentType = ContentType.objects.get_for_model(Event)
    # all_tags_event = Tag.objects.filter(
    #     taggit_taggeditem_items__content_type=contentType).distinct()
    team = Team.objects.filter(name_id=team_name_id)
    context = {
        'team': team[0],
        'active_sidebar': 'teams',
    }

    if request.method == "POST":
        if request.POST['new_project_name']:
            new_project_name = request.POST['new_project_name']
            new_project_about = request.POST['new_project_about']
            new_project_private = 'new_project_private' in request.POST
            new_project = TeamProject(name=new_project_name,
                                      about=new_project_about, private=new_project_private)
            new_project.save()

            team[0].projects.add(new_project.id)
            team.update()

        return HttpResponseRedirect('/teams/' + team[0].name_id)
    return render(request, 'about_team_page/add_team_project.htm', context)
