import re
from allauth.account.adapter import DefaultAccountAdapter
from django.core.exceptions import ValidationError

class MyAccountAdapter(DefaultAccountAdapter):
    def clean_password(self, password, user=None):
         if re.match(r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$', password):
            return password
         else:
            raise ValidationError("Пароль должен быть не менее 6 символов и содержать хотя бы одну букву и цифру")
