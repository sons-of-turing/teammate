from django.db import models
# from django.contrib.auth.base_user import BaseUserManager
# from django.contrib.auth.base_user import AbstractBaseUser
from taggit.managers import TaggableManager
import datetime
import django.utils


class Event(models.Model):

    hackathon = "Хакатон"
    tournament = "Турнир"
    competition = "Соревнование"
    TYPE_CHOICES = (
        (hackathon, "Хакатон"),
        (tournament, "Турнир"),
        (competition, "Соревнование"),
    )

    name = models.CharField(max_length=100, null=True)
    type_of_event = models.CharField(
        max_length=15, choices=TYPE_CHOICES, default="Хакатон")
    description = models.TextField(max_length=1000)
    date_start = models.DateField(default=datetime.date.today, null=True)
    date_finish = models.DateField(default=datetime.date.today, null=True)

    city = models.CharField(max_length=150, null=True, blank=True)
    tags = TaggableManager()
    image = models.ImageField(
        upload_to="images/")
    link = models.URLField()
    is_approved = models.BooleanField(null=True)

    possible_users = models.ManyToManyField("users.User", blank=True)
    possible_teams = models.ManyToManyField("users.Team", blank=True)

    def __str__(self):
        return self.name
