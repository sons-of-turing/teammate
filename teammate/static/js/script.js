$(document).ready(function () {
    $('#id_username').attr({
        'pattern': "^[a-zA-Z0-9]+$",
        'oninvalid': "this.setCustomValidity('Допустимы латинские буквы и цифры')",
        'oninput': "setCustomValidity('')"
    });
    $('[data-toggle="tooltip"').tooltip();

    $('#findByUserSkills').prop("checked", false);

    $('#skillsFilter').change(function () {
        if ($("#skillsFilter").val().length > 0)
            $('#matchMySkills').prop("checked", false);
    });
    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

});

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function filterEvents() {
    $.ajax({
        type: 'GET',
        url: '',
        data: {
            'tags[]': $("#skillsFilter").val().length != 0 ? $("#skillsFilter").val() : 0,
            'eventType': $("#selectType").val(),
            'dateStart': $("#selectDateFirst").val(),
            'dateFinish': $("#selectDateSecond").val(),
            'matchUserSkills': $('#matchMySkills').is(':checked') ? '1' : '0'
        },
        success: function (res) {
            //console.log("YEYEYSY")
            $('#eventsFiltered').html(res);
        }

    });

};


let filterTeammates = debounce(function () {
    $.ajax({
        type: 'GET',
        url: '',
        data: {
            'selectCity': $("#selectCity").val(),
            'skills[]': $("#skillsFilter").val().length != 0 ? $("#skillsFilter").val() : 0,
            'selectTarget': $("#selectTarget").val(),
            'selectLevel': $("#selectLevel").val(),
            'searchTeammates': $("#searchTeammates").val()
        },
        success: function (res) {
            //console.log(res);
            $('#usersFiltered').html(res);
        }

    });
}, 500);


let filterTeams = debounce(function (direction = null) {
    // if ($("#skillsFilter").val().length > 0)
    //     $('#findByUserSkills').prop("checked", false);
    $.ajax({
        type: 'GET',
        url: '',
        data: {
            'skills[]': $("#skillsFilter").val().length != 0 ? $("#skillsFilter").val() : 0,
            'selectTarget': $("#selectTarget").val(),
            'orderByTCount': direction,
            'searchTeams': $("#searchTeams").val()
        },
        success: function (res) {
            //console.log(res);
            $('#teamsFiltered').html(res);
        }

    });

}, 500)


function findByUserSkills(block_id) {
    if ($('#matchMySkills').is(':checked')) {
        $('#skillsFilter').selectpicker('deselectAll');
        $('#skillsFilter').selectpicker('refresh');
    }
}

function nextSignUp(btn_id) {

    if (btn_id == 1) {
        let regex = /^[a-zA-Zа-яА-ЯЁё]+$/
        if (regex.test($('#id_first_name').val()) && regex.test($('#id_last_name').val())) {
            $('#personalData').addClass('d-none')
            $('#contactData').removeClass('d-none')
            $('#nameRegexError').addClass('d-none')
        }
        else {
            $('#nameRegexError').removeClass('d-none')
        }
    }
    else if (btn_id == 2) {
        let regex = /^[a-zA-Z0-9]+$/
        if (!regex.test($('#id_username').val())) {
            $('#incorrectUsername').removeClass('d-none')
        }
        else if ($('#id_vk').val() == '' && $('#id_tg').val() == '') {
            $('#nullSocial').removeClass('d-none')
            $('#smallSocial').addClass('d-none')
        }
        else if ($('#id_vk').val().length < 5 && $('#id_tg').val().length < 5) {
            $('#smallSocial').removeClass('d-none')
            $('#nullSocial').addClass('d-none')
        }
        else {
            $('#contactData').addClass('d-none')
            $('#settingsData').removeClass('d-none')
            $('#nullSocial').addClass('d-none')
            $('#smallSocial').addClass('d-none')

        }
    }
    else if (btn_id == 3) {
        $('#settingsData').addClass('d-none')
        $('#contactData').removeClass('d-none')
    }
    // else if (btn_id == 4) {
    //     $('#settingsData').addClass('d-none')
    //     $('#skillsData').removeClass('d-none')
    // }
    else if (btn_id == 5) {
        $('#contactData').addClass('d-none')
        $('#personalData').removeClass('d-none')
    }

}

function addEventToFavourites(cmd) {
    $.ajax({
        type: 'POST',
        url: '',
        data: { 'cmd': cmd },
        success: function (res) {
            $('#fav_btn').html(res)
        }

    });

}


function joinTeamReq(cmd) {
    $.ajax({
        type: 'POST',
        url: '',
        data: { 'cmd': cmd },
        success: function (res) {
            $('#request_btn').html(res)
        }

    });
}

function cancelJoinTeamReq(cmd) {
    $.ajax({
        type: 'POST',
        url: '',
        data: { 'cmd': cmd },
        success: function (res) {
            $('#request_btn').html(res)
        }

    });
}

function acceptJoinRequest(team_id, user_id, cmd) {
    $.ajax({
        type: 'POST',
        url: '',
        data: { 'cmd': cmd, 'team_id': team_id, 'user_id': user_id },
        success: function (res) {
            $('#notices_block').html(res)
        }

    });
}

function copyToClipboard(str) {
    let to_copy = $('<input>').val(str)
    to_copy.appendTo('body')
    to_copy.select()
    document.execCommand('copy')
    to_copy.remove()
    $('#user_tg_div').tooltip('hide')
        .attr('data-original-title', 'Copied!')
        .tooltip('show');
    setTimeout(function () {
        $('#user_tg_div').tooltip('hide')
            .attr('data-original-title', 'Copy')

    }, 2000);
}

function wantTakePart(cmd, type = 'user', id = '') {
    if (cmd === 1) cmd = "yes"
    $.ajax({
        type: 'POST',
        url: '',
        data: { 'want': cmd, 'type': type, 'id': id },
        success: function (res) {
            $('#choosePopover').popover('hide')
            $('#wantBlock').html(res)
        }

    });
}