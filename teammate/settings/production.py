from .base import *
from urllib3.util import parse_url

DEBUG = True
ALLOWED_HOSTS = ['teammatedev.herokuapp.com']


DATABASE_URL = os.environ.get('DATABASE_URL')
SECRET_KEY = os.environ.get('DATABASE_URL')

EMAIL_USE_TLS = os.environ.get('EMAIL_USE_TLS')


# EMAIL_USE_SSL = os.environ.get('EMAIL_USE_SSL')

EMAIL_HOST = os.environ.get('EMAIL_HOST')
DEFAULT_FROM_EMAIL = os.environ.get('DEFAULT_FROM_EMAIL')
DEFAULT_TO_EMAIL = os.environ.get('DEFAULT_TO_EMAIL')

parsed_url = parse_url(DATABASE_URL)
name = parsed_url.path[1:]
username, password = parsed_url.auth.split(':')
host = parsed_url.host
port = parsed_url.port


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': name,
        'USER': username,
        'PASSWORD': password,
        'HOST': host,
        'PORT': port,
    }
}
