from django.contrib import admin
from .models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ["id", "name",
                    "description", "link", "is_approved"]
    list_filter = ["is_approved"]
