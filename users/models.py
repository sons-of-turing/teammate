from django.db import models

from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.base_user import AbstractBaseUser
from taggit.managers import TaggableManager
from teammate.models import Event
from datetime import datetime


class UserManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError("Users must have an email address")
        if not username:
            raise ValueError("Users must have an username")

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            username=username,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    username = models.CharField(
        max_length=30, unique=True)
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    about = models.TextField(null=True, blank=True)
    vk = models.CharField(max_length=50, blank=True, null=True)
    tg = models.CharField(max_length=50, blank=True, null=True)
    tags = TaggableManager(blank=True)

    target_project = "Создание проекта"
    target_event = "Участие в мероприятиях"
    TARGET_CHOICES = (
        (target_project, "Создание проекта"),
        (target_event, "Участие в мероприятиях"),
    )
    target = models.CharField(choices=TARGET_CHOICES, max_length=50,
                              null=True, blank=True)

    birth_date = models.DateField(blank=True, null=True)
    image = models.ImageField(upload_to="profile_images/", blank=True)
    favourite_events = models.ManyToManyField(Event, blank=True)
    date_joined = models.DateTimeField(
        verbose_name='date joined', auto_now_add=True)
    city = models.CharField(max_length=50, null=True, blank=True)
    is_new_notice = models.BooleanField(default=False)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', ]

    objects = UserManager()

    @property
    def age(self):
        if self.birth_date:
            return int((datetime.now().date() - self.birth_date).days / 365.25)
        else:
            return None

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_superuser


class TeamProject(models.Model):
    name = models.CharField(max_length=100)
    about = models.TextField()
    private = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Team(models.Model):

    name = models.CharField(max_length=100)
    target_project = "Создание проекта"
    target_event = "Участие в мероприятиях"
    TARGET_CHOICES = (
        (target_project, "Создание проекта"),
        (target_event, "Участие в мероприятиях"),
    )

    name_id = models.CharField(max_length=15, unique=True)
    users = models.ManyToManyField(User)
    projects = models.ManyToManyField(TeamProject, blank=True)
    admin = models.CharField(max_length=30, null=True)
    required_skills = TaggableManager()
    target = models.CharField(choices=TARGET_CHOICES, max_length=50,
                              null=True, blank=True)

    @property
    def count_of_members(self):
        return len(self.users.all())

    def __str__(self):
        return self.name
