from django.contrib import admin
from .models import User, Team, TeamProject


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'age', 'is_new_notice', 'email', 'first_name',
                    'last_name', 'tags', 'is_superuser')


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ['id', 'name_id', 'name',
                    'count_of_members', 'admin', 'target']


@admin.register(TeamProject)
class TeamProjectAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'private']
