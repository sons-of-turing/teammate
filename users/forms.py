from allauth.account.forms import SignupForm
from django import forms
from django.core.validators import RegexValidator, MinLengthValidator
from django.core.exceptions import ValidationError
from .models import User
from django_select2.forms import Select2Widget, Select2MultipleWidget
from django.contrib.contenttypes.models import ContentType
from taggit.models import Tag
from cities_light.models import City


class CustomSignupForm(SignupForm):
    first_name = forms.CharField(min_length=1, max_length=30, label='Имя', widget=forms.TextInput(attrs={'placeholder': 'Ваше имя', 'class': 'form-control'}), validators=[
        RegexValidator('^[a-zA-Zа-яА-яЁё]+$', message="В имени и фамилии резрешены только буквы.")])
    last_name = forms.CharField(min_length=1, max_length=30, label='Фамилия', widget=forms.TextInput(attrs={'placeholder': 'Ваша фамилия', 'class': 'form-control'}), validators=[
                                RegexValidator('^[a-zA-Zа-яА-яЁё]+$', message="В имени и фамилии резрешены только буквы.")])
    # tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.filter(
    #     taggit_taggeditem_items__content_type=ContentType.objects.get_for_model(User)).distinct(), widget=Select2MultipleWidget, required=False)
    # city = forms.ModelChoiceField(
    #     queryset=City.objects.all(), widget=Select2Widget)
    birth_date = forms.DateField(widget=forms.TextInput(
        attrs={"type": "date", 'class': 'form-control datepicker', 'min': 'min="1900-01-01"'}), required=True, input_formats=['%Y-%m-%d'])
    tg = forms.CharField(max_length=50, required=False, label='Telegram', widget=forms.TextInput(attrs={'placeholder': 'username', 'class': 'form-control'}), validators=[
        RegexValidator('^[\.\_a-zA-Z0-9]+$', message="Неправильный Telegram ID.")])
    vk = forms.CharField(max_length=50, required=False, label='ВКонтакте', widget=forms.TextInput(attrs={'placeholder': 'ID ВКонтакте', 'class': 'form-control'}), validators=[
        RegexValidator('^[\.\_a-zA-Z0-9]+$', message="Неправильный VK ID.")])

    def save(self, request):
        user = super(CustomSignupForm, self).save(request)
        # user.city = self.cleaned_data['city'].alternate_names.split(';')[0]
        user.birth_date = self.cleaned_data['birth_date']
        user.tg = self.cleaned_data['tg']
        user.vk = self.cleaned_data['vk']
        user.save()

        # print(self.cleaned_data['city'])
        # for tag in self.cleaned_data['tags']:
        #     user.tags.add(tag)

        return user
